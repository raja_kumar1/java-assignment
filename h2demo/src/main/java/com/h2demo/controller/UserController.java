package com.h2demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.h2demo.model.User;
import com.h2demo.service.UserService;

@RestController
@RequestMapping("user")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping(value = "/create")
	public ResponseEntity<User> storeUser(@RequestBody User user) {
		User user2 = userService.createUser(user);
		return new ResponseEntity<User>(user2, HttpStatus.CREATED);

	}

	@GetMapping(value = "/read {userid}")
	public ResponseEntity<User> readByUserId(@PathVariable int userId) {
		User user = userService.readUserById(userId);

		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@GetMapping(value = "reaAll")
	public ResponseEntity<List<User>> readAll() {

		return new ResponseEntity<List<User>>(userService.readAllUser(), HttpStatus.OK);

	}

}
