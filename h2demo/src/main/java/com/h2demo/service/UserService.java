package com.h2demo.service;

import java.util.List;

import com.h2demo.model.User;

public interface UserService {

	public abstract User createUser(User user);

	public abstract User readUserById(int arg);

	public abstract List<User> readAllUser();

}
