package com.h2demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.h2demo.model.User;
import com.h2demo.repository.UserRepository;

@Service
public class UserServiceImp implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional
	public User createUser(User user) {

		return userRepository.save(user);
	}

	@Override
	@Transactional
	public User readUserById(int arg) {
		
		User user =null;
		
		Optional<User> optional= userRepository.findById(arg);
		if(optional.isPresent())
		{
			user = optional.get();
		}
		
		return user;
	}

	@Override
	public List<User> readAllUser() {
		
		return userRepository.findAll();
	}

}
