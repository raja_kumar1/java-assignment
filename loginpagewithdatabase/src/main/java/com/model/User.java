package com.model;

public class User {

	private int userId;
	private String userName;
	private String address;
	private float salary;

	/**
	 * 
	 */
	public User() {
		super();
	}

	/**
	 * @param userId
	 * @param userName
	 * @param address
	 * @param salary
	 */
	public User(int userId, String userName, String address, float salary) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.address = address;
		this.salary = salary;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

}
