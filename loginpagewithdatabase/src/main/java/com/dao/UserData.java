package com.dao;

import com.model.User;

public interface UserData {

	public abstract User createUserbyId(User user);

	public abstract User readUserDatabyId(int id);

	public abstract User readUserDatabyName(String name);

	public abstract User updateUserbyId(int id);

	public abstract User deleteUserbyId(int id);

	public abstract User deleteUserByName(String name);

}
