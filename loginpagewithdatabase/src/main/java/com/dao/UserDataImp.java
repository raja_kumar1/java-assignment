package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.model.User;

public class UserDataImp implements UserData {

	@Override
	public User createUserbyId(User user) {

		return null;
	}

	@Override
	public User readUserDatabyId(int id) {

		ResultSet resultSet;
		String query = "select * from users where users_id = ? ";
		Connection connection = TestSql.getConnection();
		User user = new User();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				System.out.println("User id : - " + resultSet.getInt("users_id"));
				System.out.println("User Name : - " + resultSet.getString("users_name"));
				
//				user.setUserId(resultSet.getInt("users_id"));
//				user.setUserName(resultSet.getString("users_name"));
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return user;
	}

	@Override
	public User updateUserbyId(int id) {

		return null;
	}

	@Override
	public User deleteUserbyId(int id) {

		return null;
	}

	@Override
	public User deleteUserByName(String name) {

		return null;
	}

	@Override
	public User readUserDatabyName(String name) {

		ResultSet resultSet;
		String query = "select * from users where users_name = ? ";
		Connection connection = TestSql.getConnection();
		User user1 = new User();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, name);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				System.out.println("User id: - " + resultSet.getInt("users_id"));
				System.out.println("User Name: - " + resultSet.getString("users_name"));
				System.out.println("User City : - " + resultSet.getString("users_city"));
				System.out.println("User Phone no. : - " + resultSet.getInt("users_phoneNo"));

				user1.setUserId(resultSet.getInt("users_id"));
				user1.setUserName(resultSet.getString("users_name"));
				user1.setAddress(resultSet.getString("uaers_city"));

			}

		} catch (SQLException e) {
			System.err.println("Data not Found. ");
			e.printStackTrace();
		}
		//
		return user1;
	}

}
