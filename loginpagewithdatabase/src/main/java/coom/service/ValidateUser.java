package coom.service;

import com.model.User;

public interface ValidateUser {

	// public abstract String validationOfUserIdAndPassword(String userId, String
	// password);

	public abstract User validationOfUserIdAndPassword(String userId, String password);
}
