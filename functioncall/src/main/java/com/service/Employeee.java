package com.service;

public class Employeee {

	public int eNo;
	public String eName, eDept;
	private double eSalary;

	public double geteSalary() {
		return eSalary;
	}

	public void seteSalary(double eSalary) {
		this.eSalary = eSalary;
	}

	public void display() {

		if (eNo == 1234) {
			System.out.println("Employee number - " + eNo);
			System.out.println("Employee Name - " + eName);
			System.out.println("Employee Department - " + eDept);
			System.out.println("Employee Salary - " + eSalary);
		} else
			System.out.println(" Invalid Details");
	}

}
