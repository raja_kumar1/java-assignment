package com.main;

import java.util.Scanner;

import com.service.ValidateUser;
import com.service.Validation;

public class Main {

	public static void main(String[] args) {

		ValidateUser validateUser = new Validation();

		Scanner scanner = new Scanner(System.in);
		String userId, password;

		System.out.println("Enter user name:- ");
		userId = scanner.nextLine();

		System.out.println("Enter password:- ");
		password = scanner.nextLine();

		validateUser.validationOfUserIdAndPassword(userId, password);

		validateUser = null;
		scanner.close();
	}

}
