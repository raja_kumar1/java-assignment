package com.main;

import com.model.Emp;
import com.service.EmpAddress;

public class EmpMain {

	public static void main(String[] args) {

		EmpAddress address1 = new EmpAddress("Lucknow ,", "UP ,", "India");
		EmpAddress address2 = new EmpAddress("Patna ,", "Bihar ,", "India");

		Emp emp = new Emp(101, "Varun", address1);
		Emp emp2 = new Emp(201, "Arun", address2);

		emp.display();
		emp2.display();
	}

}
