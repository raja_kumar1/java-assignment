package com.main;

import com.model.Author;
import com.service.Book;

public class MainApp {
	public static void main(String[] args) {

		Author author = new Author("John", 42, "USA");
		Book book = new Book("Java for Begginer", 800, author);
		System.out.println("Book Name: " + book.name);
		System.out.println("Book Price: " + book.price);
		System.out.println("------------author Details----------");
		System.out.println("author Name: " + book.author.authorName);
		System.out.println("author Age: " + book.author.age);
		System.out.println("author place: " + book.author.place);
	}

}
