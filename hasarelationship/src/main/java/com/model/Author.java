package com.model;

public class Author {

	public String authorName;
	public int age;
	public String place;

	// Author class constructor
	public Author(String name, int age, String place) {
		this.authorName = name;
		this.age = age;
		this.place = place;
	}

}
