package com.model;

import com.service.EmpAddress;

public class Emp {
	int id;
	public String name;
	public EmpAddress address;

	 public Emp(int id, String name, EmpAddress address) {
		this.id = id;
		this.name = name;
		this.address = address;
	}

	public void display() {
		System.out.println(id + " " + name);
		System.out.println(address.city + " " + address.state + " " + address.country);
	}

}
