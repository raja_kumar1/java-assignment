package com.service;

public class EmpAddress {

	public String city;
	public String state;
	public String country;

	public EmpAddress(String city, String state, String country) {
		this.city = city;
		this.state = state;
		this.country = country;
	}
}