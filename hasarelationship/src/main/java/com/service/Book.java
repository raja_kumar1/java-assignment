package com.service;

import com.model.Author;

public class Book {

	public String name;
	public int price;
	// author details
	public Author author;

	public Book(String name, int price, Author author) {
		this.name = name;
		this.price = price;
		this.author = author;
	}

}
