package com.hcl.pp.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author raja_kumar
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL) // used to ignore null fields in an object
public class PetDto implements Serializable {

	private static final long serialVersionUID = 3929315775436982024L;

	private Long id;

	private String name;

	private Integer age;

	private String place;

	private Long userId;

	/**
	 * 
	 */
	public PetDto() {
		super();
	}

	/**
	 * @param id
	 * @param name
	 * @param age
	 * @param place
	 * @param userId
	 */
	public PetDto(Long id, String name, Integer age, String place, Long userId) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.place = place;
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

//	public static long getSerialversionuid() {
//		return serialVersionUID;
//	}

	
}
