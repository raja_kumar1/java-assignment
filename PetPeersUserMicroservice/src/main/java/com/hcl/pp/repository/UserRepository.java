package com.hcl.pp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.pp.model.User;

/**
 * @author raja_kumar
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {

	User findByUserName(String userName);

	User findByUserNameAndUserPassword(String userName, String password);

}
