package com.hcl.pp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author raja_kumar
 *
 */

@Entity
@Table(name = "PET_USER")
@JsonInclude(JsonInclude.Include.NON_NULL)

public class User {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "USER_NAME", length = 40, nullable = false, unique = true)
	private String userName;

	@Column(name = "USER_PASSWD", length = 40, nullable = false)
	private String userPassword;

	@Transient
	private String confirmPassword;


	/**
	 * 
	 */
	public User() {
		super();
	}


	/**
	 * @param id
	 * @param userName
	 * @param userPassword
	 * @param confirmPassword
	 */
	public User(Long id, String userName, String userPassword, String confirmPassword) {
		super();
		this.id = id;
		this.userName = userName;
		this.userPassword = userPassword;
		this.confirmPassword = confirmPassword;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getUserPassword() {
		return userPassword;
	}


	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}


	public String getConfirmPassword() {
		return confirmPassword;
	}


	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	
	
	

}
