package com.hcl.pp.rest.consumer;

import java.util.List;
import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hcl.pp.dto.PetDto;
import com.hcl.pp.exception.PetPeersException;

/**
 * @author raja_kumar
 *
 */

@FeignClient(name = "PET-SERVICE")
public interface PetRestConsumer {
	
	@GetMapping("/pet/home")
	public List<PetDto> getAllPets() throws PetPeersException;

	@GetMapping("/pet/user/detail/{userId}")
	public List<PetDto> getPetsByUserId(@PathVariable("userId") Long userId) throws PetPeersException;

	@GetMapping("/pet/detail/{petId}")
	public Optional<PetDto> getPetById(@PathVariable("petId") Long petId) throws PetPeersException;

	@PutMapping("/pet/buy/pet")
	public PetDto savePetWithUser(@RequestBody PetDto petDto) throws PetPeersException;

}
