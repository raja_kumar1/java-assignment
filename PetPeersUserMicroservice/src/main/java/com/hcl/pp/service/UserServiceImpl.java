package com.hcl.pp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.pp.dto.PetDto;
import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.User;
import com.hcl.pp.repository.UserRepository;
import com.hcl.pp.rest.consumer.PetRestConsumer;
import com.hcl.pp.service.UserService;
import com.hcl.pp.validator.LoginValidator;
import com.hcl.pp.validator.UserValidator;

/**
 * @author raja_kumar
 *
 */
@Service
public class UserServiceImpl implements UserService{
	
	
	@Autowired
	UserRepository userRepository;

	@Autowired
	PetRestConsumer petRestConsumer;

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public User addUser(UserValidator userRequest) throws PetPeersException {
		User userCreated = null;

		if (!(userRequest.getPassword().equals(userRequest.getConfirmPassword()))) {
			throw new PetPeersException("Passwords do not match");
		} else {
			User userExistsWithUserName = userRepository.findByUserName(userRequest.getUserName());
			if (userExistsWithUserName != null) {
				throw new PetPeersException("User Name already in use. Please select a different User Name");
			} else {
				try {
					ModelMapper modelMapper = new ModelMapper();
					User userRequested = modelMapper.map(userRequest, User.class);
					userCreated = userRepository.save(userRequested);
					if (userCreated != null) {
						return userCreated;
					} else {
						throw new PetPeersException("User Not created");
					}
				} catch (PetPeersException e) {
					logger.error("{}", e.getMessage());
				}
			}
			return userCreated;
		}

	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public List<PetDto> getMyPets(Long userId) throws PetPeersException {
		Optional<User> user = userRepository.findById(userId);
		if (user.isPresent()) {
			List<PetDto> myPets = new ArrayList<>();
			try {
				myPets = petRestConsumer.getPetsByUserId(userId);
				if (myPets != null && myPets.size() > 0) {
					return myPets;
				} else {
					throw new PetPeersException("You don't have Pets");
				}
			} catch (PetPeersException e) {
				throw new PetPeersException("You don't have Pets");
			}
		} else {
			throw new PetPeersException("Invalid User");
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public List<PetDto> loginUser(LoginValidator loginRequest) throws PetPeersException {
		User userExists = userRepository.findByUserNameAndUserPassword(loginRequest.getUserName(),
				loginRequest.getPassword());
		List<PetDto> pets = null;
		if (userExists != null) {
			pets = petRestConsumer.getAllPets();
			if (pets != null && pets.size() > 0) {
				return pets;
			} else {
				throw new PetPeersException("No Pets Available");
			}
		} else {
			throw new PetPeersException("Either User Name or Password or both are invalid");
		}

	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public PetDto buyPet(Long userId, Long petId) throws PetPeersException {
		PetDto petWithOwnerCreated = null;
		Optional<User> user = userRepository.findById(userId);
		if (user.isPresent()) {
			Optional<PetDto> pet = petRestConsumer.getPetById(petId);
			if (pet.isPresent()) {
				if (pet.get().getUserId() != null) {
					throw new PetPeersException("Pet Already Purchased Try with another Pet(PetId)");
				} else {
					pet.get().setUserId(userId);
					petWithOwnerCreated = petRestConsumer.savePetWithUser(pet.get());
					if (petWithOwnerCreated != null) {
						return petWithOwnerCreated;
					} else {
						throw new PetPeersException("Entry Not created");
					}
				}
			} else {
				throw new PetPeersException("Pet Not exists with Given Id");
			}
		} else {
			throw new PetPeersException("User Not exists with Given Id");
		}

	}

	@Override
	public User getUserDetailByUserId(Long userId) throws PetPeersException {
		Optional<User> user = userRepository.findById(userId);
		if (user.isPresent()) {
			return user.get();
		} else {
			throw new PetPeersException("User Not exists with Given Id");

		}
	}

	

	
	

}
