package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.dto.PetDto;
import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.User;
import com.hcl.pp.validator.LoginValidator;
import com.hcl.pp.validator.UserValidator;

/**
 * @author raja_kumar
 *
 */
public interface UserService {
	
	
	public User addUser(UserValidator userValidator) throws PetPeersException;

	public PetDto buyPet(Long userId, Long petId) throws PetPeersException;

	public List<PetDto> getMyPets(Long userId) throws PetPeersException;

	public List<PetDto> loginUser(LoginValidator loginRequest) throws PetPeersException;

	public User getUserDetailByUserId(Long userId) throws PetPeersException;

}
