package com.hcl.pp.exception;

/**
 * @author raja_kumar
 *
 */
public class PetPeersException extends Exception {

	private static final long serialVersionUID = -4497009278227746020L;

	private String customMessage;

	public PetPeersException(String customMessage) {
		super();
		this.customMessage = customMessage;
	}

	public String getCustomMessage() {
		return customMessage;
	}

	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}

	@Override
	public String getMessage() {
		return customMessage;
	}

}
