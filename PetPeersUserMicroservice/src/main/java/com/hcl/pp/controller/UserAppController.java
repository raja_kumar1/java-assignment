package com.hcl.pp.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hcl.pp.dto.PetDto;
import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.User;
import com.hcl.pp.service.UserService;
import com.hcl.pp.validator.LoginValidator;
import com.hcl.pp.validator.UserValidator;

/**
 * @author raja_kumar
 *
 */
public class UserAppController {

	@Autowired
	private UserService userService;

	private static final Logger logger = LogManager.getLogger(UserAppController.class);

	@PostMapping("/add")
	public ResponseEntity<Object> addUser(@Valid @RequestBody UserValidator userRequest) throws PetPeersException {
		User user = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			user = userService.addUser(userRequest);
			if (user != null) {
				responseEntity = new ResponseEntity<Object>(user, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Object>(user, HttpStatus.BAD_REQUEST);
			}
		} catch (PetPeersException e) {

			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@PostMapping("/login")
	public ResponseEntity<Object> loginUser(@Valid @RequestBody LoginValidator loginRequest) throws PetPeersException {
		List<PetDto> pets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			pets = userService.loginUser(loginRequest);
			if (pets != null) {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@GetMapping("/my/pets/{userId}")
	public ResponseEntity<Object> myPets(
			@PathVariable("userId") @Min(value = 1, message = "Minimum entry must be One") Long userId)
			throws PetPeersException {
		List<PetDto> pets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			pets = userService.getMyPets(userId);
			if (pets != null && pets.size() > 0) {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@PutMapping("/buy/pet/{userId}/{petId}")

	public ResponseEntity<Object> buyPet(
			@PathVariable("userId") @Min(value = 1, message = "Minimum entry must be One") Long userId,
			@PathVariable("petId") @Min(value = 1, message = "Minimum entry must be One") Long petId)
			throws PetPeersException {
		PetDto userBoughtPets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			userBoughtPets = userService.buyPet(userId, petId);
			if (userBoughtPets != null) {
				responseEntity = new ResponseEntity<Object>(userBoughtPets, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Object>(userBoughtPets, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@GetMapping("/{userId}")
	public ResponseEntity<Object> getUserByUserId(
			@PathVariable("userId") @Min(value = 1, message = "Minimum entry must be One") Long userId)
			throws PetPeersException {
		User user = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			user = userService.getUserDetailByUserId(userId);
			if (user != null) {
				responseEntity = new ResponseEntity<Object>(user, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(user, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	public ResponseEntity<Object> petServiceNotAvailable(Long userId, Long petId) {
		return new ResponseEntity<Object>("Pet-Service-Not-Available", HttpStatus.NOT_FOUND);
	}

}
