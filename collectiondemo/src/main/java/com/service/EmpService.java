package com.service;

import java.util.Set;

import com.exception.EmpException;
import com.model.Employee;

public interface EmpService {
	Employee searchEmployeeById(Set<Employee> set, int id) throws EmpException;

	Employee searchEmployeeByName(Set<Employee> set, String ename) throws EmpException;

	Employee totalSalary(Set<Employee> set, float salary) throws EmpException;
}
