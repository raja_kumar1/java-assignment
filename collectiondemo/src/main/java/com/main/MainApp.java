package com.main;

import java.util.HashSet;
import java.util.Set;

import com.exception.EmpException;
import com.exception.EmpServiceImp;
import com.model.Employee;
import com.service.EmpService;

public class MainApp {

	public static void main(String[] args) {
		Employee employee1 = new Employee(001, "Raja", 1010.32f);
		Employee employee2 = new Employee(002, "Raj", 2010.62f);
		Employee employee3 = new Employee(003, "Rajiv", 3010.52f);
		Employee employee4 = new Employee(004, "Ravi", 4010.42f);

		Set<Employee> employees = new HashSet<Employee>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		employees.add(employee4);

		EmpService empService = new EmpServiceImp();

		try {
			Employee employee = empService.searchEmployeeById(employees, 004);
			System.out.println("Employee Name:- " + employee.getEmpName());
			System.out.println("Employee Id:-  " + employee.getEmpId());
			System.out.println("Employee Salary :- " + employee.getSalary());

		} catch (EmpException e) {

			System.err.println(e.getMessage());
		}

	}

}
