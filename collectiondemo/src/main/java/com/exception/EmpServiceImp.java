package com.exception;

import java.util.Set;

import com.model.Employee;
import com.service.EmpService;

public class EmpServiceImp implements EmpService {

	public Employee searchEmployeeById(Set<Employee> set, int id){
		Employee dummyemp = null;

		for (Employee employee : set) {
			if (employee.getEmpId() == id) {
				dummyemp = employee;
			}
		}

		return dummyemp;
	}

	public Employee searchEmployeeByName(Set<Employee> set, String ename) throws EmpException {

		Employee dummyemp = null;

		for (Employee employee : set) {
			if (employee.getEmpName().equals(ename)) {
				dummyemp = employee;
			}
		}

		return dummyemp;

	}

	public Employee totalSalary(Set<Employee> set, float salary) throws EmpException {
		
		
		
		
		
		
		return null;
	}

}
