package com.exception;

public class EmpException extends Exception {

	private String msg;

	/**
	 * @param msg
	 */
	public EmpException(String msg) {
		super();
		this.msg = msg;
	}

	@Override
	public String getMessage() {
		return this.msg;
	}

}
