package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.model.User;

public class UserDataImp implements UserData {

	@Override
	public User createUserbyId(User user) {

		return null;
	}

	@Override
	public User readUserDatabyId(int id) {
		ResultSet resultSet;
		String query = "select * from users where users_id = ? ";
		Connection connection = TestSql.getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				System.out.println("User id: - " + resultSet.getInt("users_id"));
				System.out.println("User Name: - " + resultSet.getString("users_name"));
				System.out.println("User City : - " + resultSet.getString("users_city"));
				System.out.println("User Phone no. : - " + resultSet.getInt("users_phoneNo"));

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return null;
	}

	@Override
	public List<User> readUserAllbyId() {
		

		return null;
	}

	@Override
	public User updateUserbyId(int id) {

		ResultSet resultSet;
		String query = "update users set users_name = 'Dhawan'where users_id = ?";
		Connection connection = TestSql.getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				System.out.println("User id: - " + resultSet.getInt("users_id"));
				System.out.println("User Name: - " + resultSet.getString("users_name"));
				System.out.println("User City : - " + resultSet.getString("users_city"));
				System.out.println("User Phone no. : - " + resultSet.getInt("users_phoneNo"));

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return null;
	}

	@Override
	public User deleteUserbyId(int id) {

		return null;
	}

	@Override
	public User deleteUserByName(String name) {

		return null;
	}

	@Override
	public User readUserDatabyname(String name) {

		ResultSet resultSet;
		String query = "select * from users where users_name = ? ";
		Connection connection = TestSql.getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, name);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				System.out.println("User id: - " + resultSet.getInt("users_id"));
				System.out.println("User Name: - " + resultSet.getString("users_name"));
				System.out.println("User City : - " + resultSet.getString("users_city"));
				System.out.println("User Phone no. : - " + resultSet.getInt("users_phoneNo"));

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return null;
	}

}
