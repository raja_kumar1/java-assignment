package com.dao;

import java.util.List;

import com.model.User;

public interface UserData {

	public abstract User createUserbyId(User user);

	public abstract User readUserDatabyId(int id);

	public abstract User readUserDatabyname(String name);

	public abstract List<User> readUserAllbyId();

	public abstract User updateUserbyId(int id);

	public abstract User deleteUserbyId(int id);

	public abstract User deleteUserByName(String name);



}
