package com.hcl.pp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author raja_kumar
 *
 */
@Entity
@Table(name = "PETS")
@JsonInclude(JsonInclude.Include.NON_NULL)

public class Pet {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "PET_NAME", length = 55, nullable = false)
	private String name;

	@Column(name = "PET_AGE")
	private Integer age;

	@Column(name = "PET_PLACE", length = 55)
	private String place;

	@Column(name = "USER_ID")
	private Long userId;

	/**
	 * 
	 */
	public Pet() {
		super();
	}

	/**
	 * @param id
	 * @param name
	 * @param age
	 * @param place
	 * @param userId
	 */
	public Pet(Long id, String name, Integer age, String place, Long userId) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.place = place;
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	
	
}
