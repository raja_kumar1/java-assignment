package com.hcl.pp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hcl.pp.dto.UserDto;
import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.service.PetService;
import com.hcl.pp.validator.PetValidator;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

public class PetController {

	
	@Autowired
	private PetService petService;
	
	private static final Logger logger = LogManager.getLogger(PetController.class);

	@GetMapping("/home")
	public ResponseEntity<Object> petHome() throws PetPeersException {
		List<Pet> pets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			pets = petService.getAllPets();
			if (pets != null && pets.size() > 0) {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@GetMapping("/detail/{petId}")
	public ResponseEntity<Object> petDetailByPetId(
			@PathVariable("petId") @Min(value = 1, message = "Minimum entry must be One") Long petId)
			throws PetPeersException {
		Optional<Pet> petDetail = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			petDetail = petService.getPetByPetId(petId);
			if (petDetail != null) {
				responseEntity = new ResponseEntity<Object>(petDetail, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(petDetail, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@PostMapping("/add")
	public ResponseEntity<Object> addPet(@Valid @RequestBody PetValidator petRequest) throws PetPeersException {
		Pet pets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			pets = petService.savePet(petRequest);
			if (pets != null) {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@GetMapping("/user/detail/{userId}")
	public ResponseEntity<Object> petDetailByUserId(
			@PathVariable("userId") @Min(value = 1, message = "Minimum entry must be One") Long userId)
			throws PetPeersException {
		List<Pet> petDetail = new ArrayList<>();
		ResponseEntity<Object> responseEntity = null;
		try {
			petDetail = petService.getPetByUserId(userId);
			if (petDetail != null) {
				responseEntity = new ResponseEntity<Object>(petDetail, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(petDetail, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@PutMapping("/buy/pet")
	public ResponseEntity<Object> buyPet(@Valid @RequestBody Pet petRequest) throws PetPeersException {
		Pet pets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			pets = petService.savePetWithUser(petRequest);
			if (pets != null) {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@GetMapping("/{userId}")
	@HystrixCommand(fallbackMethod = "userServiceNotAvailable")
	public ResponseEntity<Object> userDetailByUserId(
			@PathVariable("userId") @Min(value = 1, message = "Minimum entry must be One") Long userId)
			throws PetPeersException {
		UserDto userDetail = new UserDto();
		ResponseEntity<Object> responseEntity = null;
		try {
			userDetail = petService.getUserDetailByUserId(userId);
			if (userDetail != null) {
				responseEntity = new ResponseEntity<Object>(userDetail, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(userDetail, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	public ResponseEntity<Object> userServiceNotAvailable(Long userId) {
		return new ResponseEntity<Object>("User-Service-Not-Available", HttpStatus.EXPECTATION_FAILED);
	}

}
