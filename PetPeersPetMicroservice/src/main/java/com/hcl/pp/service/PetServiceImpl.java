package com.hcl.pp.service;


import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.pp.dto.UserDto;
import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.repository.PetRepository;
import com.hcl.pp.rest.consumer.UserRestConsumer;
import com.hcl.pp.service.PetService;
import com.hcl.pp.validator.PetValidator;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetRepository petRepository;

	@Autowired
	private UserRestConsumer userRestConsumer;

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public Pet savePet(PetValidator petRequest) throws PetPeersException {
		Pet petCreated = null;
		ModelMapper modelMapper = new ModelMapper();
		Pet petRequested = modelMapper.map(petRequest, Pet.class);
		petCreated = petRepository.save(petRequested);
		if (petCreated != null) {
			return petCreated;
		} else {
			throw new PetPeersException("Pet Not created");
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public List<Pet> getAllPets() throws PetPeersException {
		List<Pet> pets = petRepository.findAll();
		if (pets != null && pets.size() > 0) {
			return pets;
		} else {
			throw new PetPeersException("No Pets Available");
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public Optional<Pet> getPetByPetId(Long petId) throws PetPeersException {
		Optional<Pet> pet = petRepository.findById(petId);
		if (pet.isPresent()) {
			return pet;
		} else {
			throw new PetPeersException("No pet exists with given pet id");
		}
	}

	@Override
	public List<Pet> getPetByUserId(Long userId) throws PetPeersException {
		List<Pet> pets = petRepository.findByUserId(userId);
		if (pets != null && pets.size() > 0) {
			return pets;
		} else {
			throw new PetPeersException("You Don't have pets go buy some");
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public Pet savePetWithUser(Pet petExists) throws PetPeersException {
		Pet petCreated = null;
		ModelMapper modelMapper = new ModelMapper();
		Pet petRequested = modelMapper.map(petExists, Pet.class);
		petCreated = petRepository.save(petRequested);
		if (petCreated != null) {
			return petCreated;
		} else {
			throw new PetPeersException("Pet Not created");
		}
	}

	@Override
	public UserDto getUserDetailByUserId(Long userId) throws PetPeersException {
		UserDto user = userRestConsumer.getUserDetailByUserId(userId);
		if (user != null) {
			return user;
		} else {
			throw new PetPeersException("No user Exists");
		}

	}
}
