package com.hcl.pp.service;

import java.util.List;
import java.util.Optional;

import com.hcl.pp.dto.UserDto;
import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.validator.PetValidator;

public interface PetService  {
	
	public Pet savePet(PetValidator petValidator) throws PetPeersException;

	public List<Pet> getAllPets() throws PetPeersException;

	public Optional<Pet> getPetByPetId(Long petId) throws PetPeersException;

	public List<Pet> getPetByUserId(Long userId) throws PetPeersException;

	public Pet savePetWithUser(Pet petExists) throws PetPeersException;

	public UserDto getUserDetailByUserId(Long userId) throws PetPeersException;
	

}
