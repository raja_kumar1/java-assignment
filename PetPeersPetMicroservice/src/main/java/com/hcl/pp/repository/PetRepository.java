package com.hcl.pp.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.pp.model.Pet;

public interface PetRepository extends JpaRepository<Pet, Long>{
	
	List<Pet> findByUserId(Long userId);

}
