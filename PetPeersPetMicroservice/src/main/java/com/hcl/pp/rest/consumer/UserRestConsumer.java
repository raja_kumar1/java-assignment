package com.hcl.pp.rest.consumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.hcl.pp.dto.UserDto;
import com.hcl.pp.exception.PetPeersException;

@FeignClient(name = "USER-SERVICE")
public interface UserRestConsumer {
	@GetMapping("/user/{userId}")
	public UserDto getUserDetailByUserId(@PathVariable("userId") Long userId) throws PetPeersException;
	

}
