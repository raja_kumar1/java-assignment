package com.hcl.pp.validator;

import java.io.Serializable;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/**
 * @author raja_kumar
 *
 */
public class PetValidator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -360973939414277761L;

	
	@NotNull(message = "pet Name can't be null")
	@NotEmpty(message = "pet Name can't be empty")
	private String name;

	@NotNull(message = "Pet Age Can't be null")
	@Digits(integer = 2, fraction = 0, message = "Age must be in Number ")
	@Min(value = 0, message = "age must be minimum 0 years")
	@Max(value = 99, message = "age must be maximum upto 99 years")
	private Integer age;

	@NotNull(message = "place can't be null")
	@NotEmpty(message = "place can't be empty")
	private String place;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

}
