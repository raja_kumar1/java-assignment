package com.hcl.pp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniprojectdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniprojectdemoApplication.class, args);
	}

}
