package com.hcl.pp.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.service.PetService;
import com.hcl.pp.service.UserService;
import com.hcl.pp.validator.LoginValidator;
import com.hcl.pp.validator.PetValidator;
import com.hcl.pp.validator.UserValidator;

/**
 * @author raja_kumar
 *
 */
public class PetsAppController {

	@Autowired
	private PetService petService;

	@Autowired
	private UserService userService;

	private static final Logger logger = LogManager.getLogger(PetsAppController.class);

	@PostMapping("/user/add")
	public ResponseEntity<Object> addUser(@Valid @RequestBody UserValidator userRequest) throws PetPeersException {
		User user = null;
		ResponseEntity<Object> responseEntity = null;

		try {
			user = new User();
			user.setUserName(userRequest.getUserName());
			user.setUserPassword(userRequest.getPassword());
			user = userService.addUser(user);
			if (user != null) {
				responseEntity = new ResponseEntity<Object>(user, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Object>(user, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.CONFLICT);
		}
		return responseEntity;
	}

	@PostMapping("/user/login")
	public ResponseEntity<Object> loginUser(@Valid @RequestBody LoginValidator loginRequest) throws PetPeersException {
		List<Pet> pets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			pets = userService.loginUser(loginRequest);
			if (pets != null) {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.CONFLICT);
		}
		return responseEntity;
	}

	@GetMapping("/pets")
	public ResponseEntity<Object> petHome() throws PetPeersException {
		List<Pet> pets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			pets = petService.getAllPets();
			if (pets != null && pets.size() > 0) {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.CONFLICT);
		}
		return responseEntity;
	}

	@GetMapping("/pets/myPets/{userId}")
	public ResponseEntity<Object> myPets(
			@PathVariable("userId") @Min(value = 1, message = "Minimum entry must be One") Long userId)
			throws PetPeersException {
		List<Pet> pets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			pets = userService.getMyPets(userId);
			if (pets != null && pets.size() > 0) {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.CONFLICT);
		}
		return responseEntity;
	}

	@GetMapping("/pets/petDetail/{petId}")
	public ResponseEntity<Object> petDetail(
			@PathVariable("petId") @Min(value = 1, message = "Minimum entry must be One") Long petId)
			throws PetPeersException {
		Optional<Pet> petDetail = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			petDetail = petService.getPetById(petId);
			if (petDetail != null) {
				responseEntity = new ResponseEntity<Object>(petDetail, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(petDetail, HttpStatus.NOT_FOUND);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.CONFLICT);
		}
		return responseEntity;
	}

	@PostMapping("/pets/addPet")
	public ResponseEntity<Object> addPet(@Valid @RequestBody PetValidator petRequest) throws PetPeersException {
		Pet pets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			pets = petService.savePet(petRequest);
			if (pets != null) {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.CONFLICT);
		}
		return responseEntity;
	}

	@PostMapping("/pets/buyPet/{userId}/{petId}")
	public ResponseEntity<Object> buyPet(
			@PathVariable("userId") @Min(value = 1, message = "Minimum entry must be One") Long userId,
			@PathVariable("petId") @Min(value = 1, message = "Minimum entry must be One") Long petId)
			throws PetPeersException {
		Pet userBoughtPets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			userBoughtPets = userService.buyPet(userId, petId);
			if (userBoughtPets != null) {
				responseEntity = new ResponseEntity<Object>(userBoughtPets, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Object>(userBoughtPets, HttpStatus.CONFLICT);
			}
		} catch (PetPeersException e) {
			logger.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.CONFLICT);
		}
		return responseEntity;
	}

}
