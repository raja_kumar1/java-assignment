package com.hcl.pp.service;

import java.util.List;
import java.util.Optional;

import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.validator.PetValidator;

public interface PetService {
	

	public Pet savePet(PetValidator petValidator) throws PetPeersException;

	public List<Pet> getAllPets() throws PetPeersException;

	public List<Pet> getPetsByOwnerId(User user) throws PetPeersException;

	public Optional<Pet> getPetById(Long petId) throws PetPeersException;

	public Pet savePetWithOwner(Pet petExists) throws PetPeersException;

}
