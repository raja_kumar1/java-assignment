package com.hcl.pp.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.repository.UserRepository;
import com.hcl.pp.validator.LoginValidator;
import com.hcl.pp.validator.UserValidator;




@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepository;

	@Autowired
	PetService petService;

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public User addUser(User user) throws PetPeersException {
		User userCreated = null;

//		if (!(user.getPassword().equals(user.getConfirmPassword()))) {
		if (!(user.getUserPassword().equals(user.getConfirmPassword()))) {
			throw new PetPeersException("Passwords do not match");
		} else {
			User userExistsWithUserName = userRepository.findByUserName(user.getUserName());
			if (userExistsWithUserName != null) {
				throw new PetPeersException("User Name already in use. Please select a different User Name");
			} else {
				try {
					ModelMapper modelMapper = new ModelMapper();
					User userRequested = modelMapper.map(user, User.class);
					userCreated = userRepository.save(userRequested);
					if (userCreated != null) {
						return userCreated;
					} else {
						throw new PetPeersException("User Not created");
					}
				} catch (PetPeersException e) {
					logger.error("{}", e.getMessage());
				}
			}
			return userCreated;
		}

	}

	@Override
	public User updateUser(Long userId, UserValidator userValidator) throws PetPeersException {
		
		return null;
	}

	@Override
	public List<User> listUsers() throws PetPeersException {
		
		return null;
	}

	@Override
	public User getUserById(Long userId) throws PetPeersException {
		
		return null;
	}

	@Override
	public void removeUser(Long userId) throws PetPeersException {
		
	}

	@Override
	public User findByUserName(String userName) throws PetPeersException {
		
		return null;
	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public List<Pet> getMyPets(Long userId) throws PetPeersException {
		User userExists = userRepository.findById(userId);
		if (userExists != null) {
			List<Pet> myPets = petService.getPetsByOwnerId(userExists);
			if (myPets != null && myPets.size() > 0) {
				return myPets;
			} else {
				throw new PetPeersException("You don't have Pets");
			}
		} else {
			throw new PetPeersException("Invalid User");
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public List<Pet> loginUser(LoginValidator loginRequest) throws PetPeersException {
		User userExists = userRepository.findByUserNameAndUserPassword(loginRequest.getUserName(),
				loginRequest.getPassword());
		List<Pet> pets = null;
		if (userExists != null) {
			pets = petService.getAllPets();
			if (pets != null && pets.size() > 0) {
				return pets;
			} else {
				throw new PetPeersException("No Pets Available");
			}
		} else {
			throw new PetPeersException("Either User Name or Password or both are invalid");
		}

	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public Pet buyPet(Long userId, Long petId) throws PetPeersException {
		Pet petWithOwnerCreated = null;
		User userExists = userRepository.findById(userId);
		if (userExists != null) {
			Optional<Pet> petExists = petService.getPetById(petId);
			if (petExists.isPresent()) {
				if (petExists.get().getOwner() != null) {
					throw new PetPeersException("Pet Already Purchased Try with another Pet(PetId)");
				} else {
					Set<Pet> pet = new HashSet<>();
					pet.add(petExists.get());
					petExists.get().setOwner(userExists);
					petWithOwnerCreated = petService.savePetWithOwner(petExists.get());
					if (petWithOwnerCreated != null) {
						return petWithOwnerCreated;
					} else {
						throw new PetPeersException("Entry Not created");
					}
				}
			} else {
				throw new PetPeersException("Pet Not exists with Given Id");
			}
		} else {
			throw new PetPeersException("User Not exists with Given Id");
		}

	}

}
