package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.validator.LoginValidator;
import com.hcl.pp.validator.UserValidator;


public interface UserService {
	

	public User addUser( User user) throws PetPeersException;

	public User updateUser(Long userId, UserValidator userValidator) throws PetPeersException;

	public List<User> listUsers() throws PetPeersException;

	public User getUserById(Long userId) throws PetPeersException;

	public void removeUser(Long userId) throws PetPeersException;

	public User findByUserName(String userName) throws PetPeersException;

	public Pet buyPet(Long userId, Long petId) throws PetPeersException;

	public List<Pet> getMyPets(Long userId) throws PetPeersException;

	public List<Pet> loginUser(LoginValidator loginRequest) throws PetPeersException;

}
