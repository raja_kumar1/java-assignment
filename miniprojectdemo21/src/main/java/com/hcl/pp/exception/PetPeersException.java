package com.hcl.pp.exception;

public class PetPeersException extends Exception {

	

	private String customMessage;

	public PetPeersException(String customMessage) {
		super();
		this.customMessage = customMessage;
	}

	public String getCustomMessage() {
		return customMessage;
	}

	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}

	public String getMessage() {
		return customMessage;
	}

}
