package com.hcl.pp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class PetPeersEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetPeersEurekaServerApplication.class, args);
	}

}
