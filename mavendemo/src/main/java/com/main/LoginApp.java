package com.main;

import java.util.Scanner;

import com.model.User;
import com.service.UserService;
import com.service.UserServiceImp;

public class LoginApp {

	public static void main(String[] args) {

		System.out.println("Welcome to Login Page: ");
		Scanner scanner = new Scanner(System.in);
		try {

			System.out.println("Enter User Id here:- ");
			int Id = scanner.nextInt();
			System.out.println("Enter Password here:- ");
			String pas = scanner.next();

			UserService service = new UserServiceImp();
			User user = service.validation(Id, pas);
//			Object user2 = service.deleteUsreBY(Id);

			if (user != null) {
				System.out.println("UserID: " + user.getUserId());
				System.out.println("UserName: " + user.getUserName());
				System.out.println("User Password: - " + user.getPassword());
			} else {
				System.err.println(" Your UserId or Password is invalid,Please try later.");
			}

			scanner.close();
			user = null;
			//user2 = null;
		} catch (Exception e) {
			System.err.println("Something worng check it now.");
		}
	}
}
