package com.dao;

import com.model.User;

public interface UserDao {

	public abstract User createUsre(String userName, String password);

	public abstract User readUsreBy(int userId);

	public abstract User updateUsre(User user);

	public abstract User deleteUsreBY(int userId);
	
	public abstract User validation(int userId,String password);
}
