package com.service;

import com.model.User;

public interface UserService {
	public abstract User createUsre(String userName, String password);

	public abstract User readUsreBy(int userId);

	public abstract User updateUsre(User user);

	public abstract int deleteUsreBY(int userId);

	public abstract User validation(int userId, String password);
}
