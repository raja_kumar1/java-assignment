package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.service.Calculator;

public class CalculatorTest {

	@Test
	public void testAdd() {
		Calculator calculator = new Calculator();

		assertEquals(5, calculator.add(4, 1));
	}

	@Test
	public void testAdd2() {
		Calculator calculator = new Calculator();

		assertEquals(6, calculator.add(4, 2));
	}

	@Test
	public void testsub() {
		Calculator calculator = new Calculator();

		assertEquals(4, calculator.sub(8, 4));
	}
	
	@Test
	public void testmul() {
		Calculator calculator = new Calculator();
		assertEquals(32, calculator.mul(4, 8));
	}

}
